# khroma-tests

Tests and demo code for the [Khroma](https://github.com/suprematic/khroma) ClojureScript library.

## Rationale

Khroma cannot be tested by itself, since a lot of the APIs it uses are only exposed by Chrome for extensions with the proper permissions. Therefore, we need a test application which runs inside Chrome.

It should also double as usage examples. You can read more about it [here](http://numergent.com/2015-10/Using-devcards-for-testing-a-ClojureScript-Chrome-extension.html) or check out all other [khroma-related articles](http://numergent.com/tags/khroma/).

Khroma-tests aims to keep pace with Khroma version numbers, so you can expect that version 0.2.0 will have tests for Khroma 0.2.0

## Running

* Clone this repository.
* `lein chromebuild once`
* Add the just built extension from `target/unpacked` to Chrome.
* Press the shiny new lambda button on your browser bar.

## Dependencies

If the latest `khroma` and `chromebuild` dependencies haven't been pushed to Clojars, you can clone and `lein install` from these repositories:

* [khroma](https://github.com/suprematic/khroma) (alternatively, [develop](https://github.com/suprematic/khroma/tree/develop) for snapshot)
* [chromebuild](https://github.com/ricardojmendez/lein-chromebuild)

## Methodology

This repository will be maintained using [git-flow](http://nvie.com/posts/a-successful-git-branching-model/). Pull requests are welcome, please make them from your clone's `develop` branch.

`develop` and `master` branches can be considered set in stone, but I'll be developing this publicly - if you see a feature branch, assume it's mercurial and may be amended or rebased in the future.


## License

Copyright (c) 2015 Numergent Limited.  Distributed under the Eclipse Public License.